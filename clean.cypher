call gds.graph.drop('package_dep');

drop constraint
on (n:package) 
assert n.name is unique;

match (n) detach delete n;
