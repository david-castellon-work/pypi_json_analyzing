#!/bin/python3
import sys
import json
import jk_pypiorgapi
import concurrent.futures
import re


def main():
    pypi_dep_file_header = ""
    pypi_packages_list = []
    pypi_dep_lines_list = []

    with open ("all_package_names.csv", "r") as all_package_names_file:
        lines = all_package_names_file.read().lower().splitlines()
        pypi_packages_list = lines[1:]
        all_package_names_file.close()

    # print(pypi_packages_list)

    with open ("pypi_dep.regex.csv", "r") as pypi_dep_file:
        # print("reading pypi_dep.csv")
        lines = pypi_dep_file.read().splitlines()
        pypi_dep_file_header = lines[0]
        pypi_dep_lines_list = lines[1:]
        pypi_dep_file.close()

    length = len(pypi_dep_lines_list) - 1
    line_counter = 0

    while line_counter < len(pypi_dep_lines_list):
        percent = 100.0 * line_counter / length
        print("percent: ", percent)

        line = pypi_dep_lines_list[line_counter]
        index = line.find(",") + 2 # extra 1 to get past first apostrophe
        package = line[0:index]
        dep_string = line[index:-1]
        # print("dep_string:", dep_string)

        dep_list = dep_string.split(",")
        # print("dep_list:", dep_list)

        dep_counter = 0
        while dep_counter < len(dep_list):
            dep = dep_list[dep_counter]
            if dep not in pypi_packages_list:
                print("package:", package)
                print("dep not in pypi_packages_list: ", dep)
                while (dep in dep_list):
                    dep_list.remove(dep)
            dep_counter += 1

        pypi_dep_lines_list[line_counter] = line[0:index] + ",".join(dep_list) + "\""
        line_counter += 1

    pypi_dep_clean_file_string = pypi_dep_file_header + "\n" + "\n".join(pypi_dep_lines_list)

    with open ("pypi_dep.clean.csv", "w") as pypi_dep_clean_file:
        pypi_dep_clean_file.write(pypi_dep_clean_file_string)
        pypi_dep_clean_file.close()

main()
