#!/bin/python3
import jk_pypiorgapi


api = jk_pypiorgapi.PyPiOrgAPI()
package_names = api.listAllPackages();

print("Number of packages: ", len(package_names));

package_names_file = open("all_package_names.csv", "w");

def format_element(element):
    return str(element[1]);

file_list = list(map(format_element, package_names))
file_list.insert(0,"name")

file_text = "\n".join(file_list)

package_names_file.write(file_text);
package_names_file.close();
