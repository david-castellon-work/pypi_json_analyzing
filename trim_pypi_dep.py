#!/bin/python3
import sys
import json
import jk_pypiorgapi
import concurrent.futures
import re

def rm_spare(line):
    new_line = re.sub(r"[\w\._-]*,\"\"$","",line)
    return new_line

def rm_spaces(line):
    new_line = re.sub(" ","",line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_single_quotes(line):
    new_line = re.sub(r"'.*?'", "", line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_parenthesis(line):
    new_line = re.sub(r"\(.*?\)", "", line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_brackets(line):
    new_line = re.sub(r"\[.*?\]", "", line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_langles(line):
    new_line = re.sub(r"<.*?(,|;|\")", r"\1", line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_rangles(line):
    new_line = re.sub(r">.*?(,|;|\")", r"\1", line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_tildas(line):
    new_line = re.sub(r"~.*?(,|;|\")", r"\1", line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_equals(line):
    new_line = re.sub(r"=.*?(,|;|\")", r"\1", line)
    # print(new_line)
    # if new_line is None: return line
    return new_line

def rm_semicolons(line):
    new_line = line
    new_line = re.sub(r";(.*?\".*?\").*?\"$", "\"", new_line)
    new_line = re.sub(r";(\".*?\".*?).*?\"", "\"", new_line)

    new_line = re.sub(r";(.*?\".*?\").*?,", r",", new_line)
    new_line = re.sub(r";(\".*?\".*?).*?,", r",", new_line)

    new_line = re.sub(r";,", r",", new_line)
    new_line = re.sub(r";\"", "\"", new_line)

    new_line = re.sub(r";.*?,", r",", new_line)
    new_line = re.sub(r";.*?\"$", "\"", new_line)
    # new_line = re.sub(r"", r"", new_line)
    return new_line

def rm_comma_runs(line):
    new_line = re.sub(r",,*", ",", line)
    new_line = re.sub(",\"$", "\"", new_line)
    return new_line


def trim(line):
    ret_line = line
    ret_line = rm_spare(ret_line)
    ret_line = rm_spaces(ret_line)
    ret_line = rm_single_quotes(ret_line)
    ret_line = rm_parenthesis(ret_line)
    ret_line = rm_brackets(ret_line)
    ret_line = rm_langles(ret_line)
    ret_line = rm_rangles(ret_line)
    ret_line = rm_tildas(ret_line)
    ret_line = rm_equals(ret_line)
    ret_line = rm_semicolons(ret_line)
    ret_line = rm_comma_runs(ret_line)
    return ret_line

def main():
    pypi_dep_lines_list = []

    with open ("pypi_dep.csv", "r") as pypi_dep_file:
        # print("reading pypi_dep.csv")
        pypi_dep_lines_list = pypi_dep_file.read().splitlines()
        pypi_dep_file.close()

    pypi_dep_lines_new_list = filter(lambda x: x!= "", list(map(trim, pypi_dep_lines_list)))
    # print(pypi_dep_lines_new_list)
    new_pypi_csv = "\n".join(pypi_dep_lines_new_list).lower()

    with open ("pypi_dep.regex.csv", "w") as pypi_dep_file:
        # print("writing pypi_dep.csv")
        pypi_dep_file.write(new_pypi_csv)
        pypi_dep_file.close()

main()
