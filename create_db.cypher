create constraint if not exists
on (n:package)
assert n.name is unique;

load csv with headers from "file:///all_package_names.csv" as row
merge (p:package {name: row.name});

load csv with headers from "file:///pypi_dep.regex.csv" as row
with row, split(row.dependencies, ",") as dependencies
unwind dependencies as dependency
match (p1:package {name: row.name}), (p2:package {name: dependency})
where p1.name <> p2.name
merge (p1)-[:DEPENDS_ON]->(p2);

match (n:package)
where not (n)--()
delete (n);

call gds.graph.create('package_dep', 'package', 'DEPENDS_ON');

call gds.degree.write('package_dep', {
    writeProperty: 'degree',
    orientation: 'REVERSE'

});

call gds.louvain.write('package_dep', {
	writeProperty: 'community'
});

match (n)-[:DEPENDS_ON *1..5]->(x)
with x, count(x) as _sup_deg
set x.super_degree = _sup_deg;

match (n)-[r:DEPENDS_ON]->(x)
set r.weight = n.super_degree;
