#!/bin/python3
import os
import sys
import json
import jk_pypiorgapi
import concurrent.futures
import threading

seperator = "--------------------------------------------------"

api = jk_pypiorgapi.PyPiOrgAPI()
write_lock = threading.Lock()

all_package_names_list = []

with open ("all_package_names.csv", "r") as all_package_names_file:
    all_package_names_list = all_package_names_file.read().splitlines()

all_package_names_list_length = len(all_package_names_list)
all_package_names_list.pop(0)
print("len(all_package_names_list) = ", all_package_names_list_length)
print("Loading all_package_names into python dictionary.")
print("Creating new all_package_dependencies.txt")

all_packages_info_list = []
package_index = 0

prefix = os.path.join(os.getcwd(), "pypi_package_jsons")

def fetch_package(package_name):

    print("Fetching package data dictionary:", package_name)

    package_data_dict = api.getPackageInfoJSON(package_name)

    global write_lock
    write_lock.acquire()

    global all_packages_info_list
    global package_index
    package_index += 1
    print(seperator)
    print(package_index)
    print(all_package_names_list_length)
    print("percent done =", str(100 * package_index / all_package_names_list_length), "%")
    print(package_name, "fetched!") 
    if package_data_dict is None:
        write_lock.release()
        return
    # print(str(package_data_dict));
    # all_packages_info_list.append(str(package_data_dict))
    # print("all package info list length: ", str(len(all_packages_info_list)))
    # print(str(all_packages_info_list))
    # print(all_packages_info_list[len(all_packages_info_list) - 1])

    # print(seperator)

    with open (os.path.join(prefix, package_name + ".txt"), "w", encoding="utf-8") as package_json_file:
        package_json_file.write(json.dumps(package_data_dict, indent=4))

    write_lock.release()
    return package_name + " loaded"

def main():
    print("Running get requests asynchronously.")

    print("prefix: ", prefix)

    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for package_name in all_package_names_list:
            futures.append(
                    executor.submit(
                        fetch_package, package_name
                    )
           )
        for future in concurrent.futures.as_completed(futures):
            print(future.result())
            print(seperator)

    global package_index
    print("Number of packages loaded:", package_index)

main()
