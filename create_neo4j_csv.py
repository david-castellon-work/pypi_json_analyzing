#!/bin/python3
import os
import json

package_json_dir = "./pypi_package_jsons/"
packages_csv_text = "./pypi_dep.csv"
delimiter = "--------------------------------------------------"

def main():
    x = 0
    percent_done = 0
    file_list = os.listdir(package_json_dir)
    file_list_length = len(file_list)
    # print(file_list)

    with open (packages_csv_text, "w") as packages_csv:

        packages_csv.write("name,dependencies\n");

        for filename in file_list:
            
            print(filename)
            x += 1
            percent_done = x / file_list_length
            filepath = package_json_dir + filename
            with open (filepath, "r") as package_file:
                package = json.load(package_file)
                print("package[\"info\"][\"name\"]= ", package["info"]["name"])
                print("package[\"info\"][\"requires_dist\"]= ", package["info"]["requires_dist"])

                name = ""
                # dependencies = ""

                name = package["info"]["name"]
                dependencies_list = package["info"]["requires_dist"]

                if dependencies_list is None:
                    dependencies = "\"\"\n"
                else:
                    dependencies = "\"" + ",".join(dependencies_list) + "\"\n"

                line = name + "," + dependencies
                packages_csv.write(line)

                print("percent done: ", percent_done)
                print(delimiter)


        print("number of files:", x)

main()
