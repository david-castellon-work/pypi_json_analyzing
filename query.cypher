match (n1:package)-[r:DEPENDS_ON]->(n2)
return n1,r,n2
limit 200;

match (n1:package)-[r:DEPENDS_ON]->(n2:package)
where 1000000 < n2.super_degree and 300000 < n1.super_degree
return n1, r, n2;

match (n1:package)-[r:DEPENDS_ON]->(n2:package)
where 100000 < n2.super_degree and 100000 < n1.super_degree
return n1, r, n2;

match (n1:package)-[r:DEPENDS_ON]->(n2:package)
where 10000 < n2.super_degree and 10000 < n1.super_degree
return n1, r, n2;
