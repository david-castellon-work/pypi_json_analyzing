# Pypi Package Dependency Extraction

The purpose of this project is to analyze the ways that pypi dependencies depend on eachother.

## Pypi Package Jsons

I've uploaded the pypi package jsons into a google drive. Retrieving them individually through gets is a long
and expensive operation and additionally may result in being blocked by the administrator of the pypi site.

Here's the link to a google drive containing the pypi package jsons I extracted. I ran the scripts on 2021-08-06.

https://drive.google.com/file/d/171ShhlLtsJUUOJjJtWZs0pd712CWPYso/view?usp=sharing

Extract the file into this directory. The extracted file should be a file called pypi\_package\_jsons
with json text files inside. Place pypi\_package\_jsons into this directory, if you want to generate
the datasets using the python scripts do the following.

	./create_neo4j_csv.py # Creates pypi_dep.csv.
	./trim_pypi_dep.py # Creates pypi_dep.regex.csv (header must be inserted manually.)
	./clean_pypi_dep.py # Creates pypi_dep.clean.csv. Removes garbage entries in dependency strings.

Here's the link to a google drive containing the csv file datasets generated from the pypi package jsons.

https://drive.google.com/file/d/1m26z33DYqR4FYRrKNzywdu675yU-S5-j/view?usp=sharing

## Pypi Package Dependecy CSV Extraction:

If you prefer to do use my scripts that uses gets. Do the following.

	mkdir pypi_package_jsons
	./create_package_names_file.py # Generates ./all_package_names.csv
	./create_all_packages_info_file.py # Generate pypi_package_jsons directory with packages jsons in it
	./create_neo4j_csv.py # Creates pypi_dep.csv.
	./trim_pypi_dep.py # Creates pypi_dep.regex.csv (header must be inserted manually.)
	./clean_pypi_dep.py # Removes garbage entries in dependency strings.


## Neo4j Instructions:

Create a neo4j database with the gds plugin installed.

Move all\_package\_names.csv and pypi\_dep.clean.csv to neo4j database import folder.
Copy the cipher script commands in create\_db.cypher into a cypher shell and execute.

Note: I've found that while finding the degree for each node is useful, it does not tell you how many
nodes depend on that node indirectly. The following query can do that.

	match (n)-[:DEPENDS_ON *1..]->(x)
	with x, count(x) as _sup_deg
	set x.super_degree = _sup_deg;

But, it is computationally expensive. So, I've been adding a cap on depth. In create\_db.cypher I use
a depth of 5, but you are more than welcome to change it.

## Neo4j Visuals:

The queries I usually run are in query.cypher and are also used in query1.html, query2.html or query3.html. After the database
is fully setup you can open up the html files to get visuals of different resolutions of the database.

## Dependecy String Trimming:

The dependency string is a list of python pypi dependencies with additional information that is also specified in the string
like version number of the package, version number of python, and more information. In trim\_pypi\_dep\_csv.py, I define
a function for each regex patterns that I saw in the dependency string to best remove those details.

If there are additional patterns I missed or patterns that need to be modified, you can add them there. Note: that there
are some garbage strings generated from this approach.

However, the dependency string file is only used to create relationships between already existing nodes (generated
from the package name list), so it is not likely that package names are spontaneously added.

To get a better dataset file with no garbage strings I compare the dependency string entries
with the entries in the package name list to remove the garbage strings.
